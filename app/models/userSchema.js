let mongoose = require("mongoose"),
  registerUserSchema = new mongoose.Schema({
    id: String,
    token: String,
    email: String,
    name: String
  });
mongoose.model("user", registerUserSchema);
