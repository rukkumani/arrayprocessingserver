const express = require("express"),
  router = express.Router();

module.exports = function(app, passport) {
  app.use("/", router);
};

function isLoggedIn(req, res, next) {
  console.log(req.isAuthenticated());
  // if user is authenticated in the session, carry on
  if (req.isAuthenticated()) return next();

  // if they aren't redirect them to the home page
  res.send({ data: "redirect" });
}

router.post("/arrayAlteration/TYPE=1", async (req, res) => {
  if (
    req.body &&
    req.body.modifyArray &&
    Array.isArray(req.body.modifyArray) &&
    req.body.modifyArray.length
  ) {
    await arrayModification(req.body.modifyArray)
      .then(
        value => {
          res.send({ data: "success", result: value });
        },
        reject => {
          res.send({ data: "error" });
        }
      )
      .catch(err => {
        res.send({
          data: "error"
        });
      });
  } else {
    res.send({
      data: "error"
    });
  }
});

async function arrayModification(modifyArray) {
  const evenArray = await modifyArray
    .filter(arr => {
      if (typeof arr == "number" && !isNaN(arr)) return arr % 2 == 0;
    })
    .sort((a, b) => a - b);

  const oddArray = await modifyArray
    .filter(inArr => {
      if (typeof inArr == "number" && !isNaN(inArr)) return inArr % 2 != 0;
    })
    .sort((a, b) => b - a);
  var result = [];
  return new Promise((resolve, reject) => {
    if (oddArray && evenArray && oddArray.length >= evenArray.length) {
      result = oddArray.reduce(function(arr, v, i) {
        if (evenArray[i]) {
          return arr.concat(v, evenArray[i]);
        } else {
          return arr.concat(v);
        }
      }, []);
      resolve(result);
    } else if (oddArray && evenArray && oddArray.length <= evenArray.length) {
      result = evenArray.reduce(function(arr, v, i) {
        if (oddArray[i]) {
          return arr.concat(oddArray[i], v);
        } else {
          return arr.concat(v);
        }
      }, []);
      resolve(result);
    } else {
      reject(0);
    }
  });
}

router.post("/stringAlteration/TYPE=2", (req, res) => {
  if (req.body && req.body.stringInput && req.body.stringInput.length) {
    reverse(req.body.stringInput);
  } else {
    res.send({ data: "error" });
  }

  function isAlphabet(x) {
    return (x >= "A" && x <= "Z") || (x >= "a" && x <= "z");
  }

  function reverse(strInput) {
    let r = strInput.length - 1,
      l = 0;

    while (l < r) {
      if (!isAlphabet(strInput[l])) l++;
      else if (!isAlphabet(strInput[r])) r--;
      else {
        String.prototype.replaceAt = function(index, replacement) {
          return (
            this.substr(0, index) +
            replacement +
            this.substr(index + replacement.length)
          );
        };
        const temp = strInput[l];
        strInput = strInput.replaceAt(l, strInput[r]);
        strInput = strInput.replaceAt(r, temp);
        l++;
        r--;
      }
    }
    res.send({ data: "success", result: strInput });
  }
});

router.post("/findMissingElement/TYPE=3", async (req, res) => {
  if (
    req.body &&
    req.body.inputArray &&
    Array.isArray(req.body.inputArray) &&
    req.body.inputArray.length
  ) {
    const inputArray = await req.body.inputArray.sort((a, b) => a - b);
    firstElem = inputArray[0];
    lastElem = inputArray[inputArray.length - 1];
    const missingElem = [];
    if (
      typeof lastElem == "number" &&
      !isNaN(lastElem) &&
      typeof firstElem == "number" &&
      !isNaN(firstElem)
    ) {
      for (let i = firstElem; i <= lastElem; i++) {
        if (inputArray.indexOf(i) == -1) {
          missingElem.push(i);
        }
      }
      res.send({ data: "success", result: missingElem });
    } else {
      res.send({ data: "error" });
    }
  } else {
    res.send({ data: "error" });
  }
});
