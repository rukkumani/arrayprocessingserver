const express = require("express"),
  router = express.Router(),
  mongoose = require("mongoose"),
  userModel = mongoose.model("user");

// app/routes.js

module.exports = function(app, passport) {
  // route for home page
  router.get("/", function(req, res) {
    res.render("https://rukkumani.github.io/arrayclient/#/login"); // load the index.ejs file
  });

  router.get("/logout", function(req, res) {
    req.logout();
    res.redirect("https://rukkumani.github.io/arrayclient/#/login");
  });

  // =====================================
  // GOOGLE ROUTES =======================
  // =====================================
  // send to google to do the authentication
  // profile gets us their basic information including their name
  // email gets their emails
  app.get(
    "/auth/google",
    passport.authenticate("google", { scope: ["profile", "email"] })
  );

  // the callback after google has authenticated the user
  app.get(
    "/auth/google/callback",
    passport.authenticate("google", {
      successRedirect: "https://rukkumani.github.io/arrayclient/#/home",
      failureRedirect: "https://rukkumani.github.io/arrayclient/#/login"
    })
  );
};
