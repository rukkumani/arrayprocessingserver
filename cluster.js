const cluster = require("cluster");
const app = require("./app");
const numCPUs = require("os").cpus().length;

cluster.schedulingPolicy = cluster.SCHED_RR;

if (cluster.isMaster) {
  for (var i = 0; i < numCPUs; i++) {
    cluster.fork();
  }

  Object.keys(cluster.workers).forEach(function(id) {
    console.log("I am running with ID : " + cluster.workers[id].process.pid);
  });

  cluster.on("exit", function(worker, code, signal) {
    cluster.fork();
    console.log("worker " + worker.process.pid + " died");
  });
} else {
  app(cluster);
}
