const path = require("path"),
  rootPath = path.normalize(__dirname + "/.."),
  chalk = require("chalk"),
  env = process.env.NODE_ENV || "development",
  dbHost = process.env.DB_HOST_NAME,
  dbPort = process.env.DB_PORT;

var config = {
  development: {
    root: rootPath,
    port: 4001 || dbPort,
    db: "mongodb://localhost/user" || dbHost
  }
};
module.exports = config[env];
logConfiguration();
function logConfiguration() {
  console.log(chalk.styles.green.open + chalk.styles.green.close);
  console.log(
    "\n\n ---------------------Configuration in Use --------------------------"
  );
  console.log(config[env]);
}
