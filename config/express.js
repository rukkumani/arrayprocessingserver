const glob = require("glob"),
  cors = require("cors"),
  logger = require("morgan"),
  cookieParser = require("cookie-parser"),
  bodyParser = require("body-parser"),
  compress = require("compression"),
  methodOverride = require("method-override"),
  session = require("express-session"),
  MongoStore = require("connect-mongo")(session),
  passport = require("passport");

module.exports = function(app, config, cluster) {
  const env = process.env.NODE_ENV || "development";
  app.locals.ENV = env;
  app.use(logger("dev"));
  app.set("views", config.root + "/app/views");
  app.set("view engine", "jade");
  app.use(bodyParser.json({ limit: "500MB" }));
  app.use(
    bodyParser.urlencoded({
      limit: "500MB",
      extended: true
    })
  );
  app.use(cookieParser());
  app.use(compress());
  app.use(methodOverride());
  app.use(cors());
  app.use(
    session({
      secret: "india",
      resave: false,
      saveUninitialized: true,
      cookie: { secure: false },
      store: new MongoStore({
        url: "mongodb://localhost/user",
        collection: "sessions"
      })
    })
  );
  require("./passport")(passport);
  // session secret
  app.use(passport.initialize());
  app.use(passport.session());
  var controllers = glob.sync(config.root + "/app/controllers/*.js");
  controllers.forEach(function(controller) {
    require(controller)(app, passport);
  });

  app.get("/", (req, res) => {
    console.log("hiiii");
  });

  app.use(function(req, res, next) {
    var err = new Error("Not Found");
    err.status = 404;
    next(err);
  });

  if (app.get("env") === "development") {
    app.use(function(err, req, res, next) {
      res.status(err.status || 500);
      res.render("error", {
        message: err.message,
        error: err,
        title: "error"
      });
    });
  }

  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render("error", {
      message: err.message,
      error: {},
      title: "error"
    });
  });
};
