const GoogleStrategy = require("passport-google-oauth").OAuth2Strategy,
  mongoose = require("mongoose"),
  userModel = mongoose.model("user");

// load up the user model

// load the auth variables
var configAuth = require("./auth");

module.exports = function(passport) {
  // used to serialize the user for the session
  passport.serializeUser(function(user, done) {
    done(null, user.id);
  });

  // used to deserialize the user
  passport.deserializeUser(function(id, done) {
    userModel.findOne({ id: id }, function(err, user) {
      done(err, user);
    });
  });

  // GOOGLE
  passport.use(
    new GoogleStrategy(
      {
        clientID: configAuth.googleAuth.clientID,
        clientSecret: configAuth.googleAuth.clientSecret,
        callbackURL: configAuth.googleAuth.callbackURL
      },
      function(token, refreshToken, profile, done) {
        process.nextTick(function() {
          // try to find the user based on their google id
          userModel.findOne({ id: profile.id }, function(err, user) {
            if (err) return done(err);

            if (user) {
              // if a user is found, log them in
              return done(null, user);
            } else {
              // if the user isnt in our database, create a new user
              var newUser = new userModel();

              // set all of the relevant information
              newUser.id = profile.id;
              newUser.token = token;
              newUser.name = profile.displayName;
              newUser.email = profile.emails[0].value; // pull the first email

              // save the user
              newUser.save(function(err) {
                if (err) throw err;
                return done(null, newUser);
              });
            }
          });
        });
      }
    )
  );
};
