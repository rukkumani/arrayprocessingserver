const express = require("express"),
  config = require("./config/config"),
  glob = require("glob"),
  mongoose = require("mongoose"),
  app = express();

module.exports = function(cluster) {
  mongoose.connect(config.db, {
    useCreateIndex: true,
    useNewUrlParser: true,
    useUnifiedTopology: true
  });
  let db = mongoose.connection;
  db.on("error", function() {
    throw new Error("unable to connect to database at " + config.db);
  });

  const models = glob.sync(config.root + "/app/models/*.js");
  models.forEach(function(model) {
    require(model);
  });

  require("./config/express")(app, config, cluster);

  app.listen(config.port, function() {
    console.log(
      "Application started! Worker %d started!, process %d",
      cluster.worker.id,
      cluster.worker.process.pid
    );
    console.log("Express server listening on port " + config.port);
  });
};
